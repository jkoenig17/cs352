#ifndef BASEBALL_H
#define BASEBALL_H

class Baseball {
 public:
  static int EvaluateDigit(char);
  static char ValueToDigit(unsigned char);
  static unsigned int FromBase(const std::string&, unsigned int);
  static std::string ToBase(unsigned int, unsigned int);
  static unsigned int EvaluateLiteral(const std::string&);
  static std::string Evaluate(const std::string&);
};

#endif
