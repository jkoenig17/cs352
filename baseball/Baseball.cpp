#include<iostream>
#include<sstream>
#include"Baseball.h"
#include<string>
#include<cmath>
using std::stringstream;

int Baseball::EvaluateDigit(char digit) {
  int number = 0;
  char value = toupper(digit);
  if (digit == '0' || digit == '1' || digit == '2' || digit == '3' || digit == '4' || digit == '5' || digit == '6' || digit == '7' || digit == '8' || digit == '9') {
    number = digit -'0';
    return number;
    //If its a letter it subracts 55 from that letter so that it gives the right value
  }else if(value >= 65 && value <= 90) {
    number = value - '7';
    return number;
  } else {
  stringstream out;
  out << "Illegal digit: " << digit;
  throw out.str();
  }
}

char Baseball::ValueToDigit(unsigned char digit) {
  char number = digit;
  if(digit < 10) {
    number = number + '0';
    return number;
  } else if(digit > 9 && digit < 36) {
    number = number + '7';
    return tolower(number);
  } else {
    stringstream out;
    out << "Illegal digit: " << digit;
    throw out.str();
  }
}

unsigned int Baseball::FromBase(const std::string& number, unsigned
				int base) {
  double length = number.length();
  double dlength = length;
  double dBase = base;
  unsigned int value = 0;
  for(int i=0; i < length; i++) {
    unsigned int d = EvaluateDigit(number.at(i));
    value = value + d * pow(dBase,dlength-1);  
    dlength--;
  }
  return value;
}

std::string Baseball::ToBase(unsigned int value, unsigned int base) {
  std::string token = "";
  if(value == 0){
    token = "0";
  } else {
    unsigned int i = value;
    while(i > 0) {
      unsigned char c = i % base;
      token = ValueToDigit(c) + token;
      i = i/base;
    }

  }
  return token;
}

unsigned int Baseball::EvaluateLiteral(const std::string& literal) {
  unsigned int value = 0;
  unsigned int flag = 0; 
  unsigned int mark = 0;
  for(unsigned int i = 0; i < literal.length(); i++) {
    if(literal.at(i) == '_') {
      mark = i;
      flag = 1;
    }
  }
  if(flag == 0) {
    value = atoi(literal.c_str());
    
  } else if(flag == 1) {
    std::string v = literal.substr(0,mark);
    std::string b = literal.substr(mark+1,literal.length());
    unsigned int base = atoi(b.c_str());
    value = FromBase(v,base);
  }

  return value;
}

std::string Baseball::Evaluate(const std::string&) {
  return NULL;
}
/*
int main(int argc, char **argv) {
  char test = 'a';
  int test2 = EvaluateDigit(test);
  std::cout << EvaluateDigit(test) << std::endl;
  return 0;
}
*/ 

